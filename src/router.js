import Vue from "vue";
import Router from "vue-router";
import Home from "./components/Home";
import Add from "./components/Add";
import SingleClub from './components/SingleClub'
import EditClub from './components/EditClub'

Vue.use(Router);

const router = new Router({
  mode: "history",
  routes: [
    { path: "/", component: Home, name: "home" },
    { path: "/add", component: Add, name: "add" },
    { path: '/club/:name', component: SingleClub, name:'single-club' },
    { path: '/edit/:name', component: EditClub, name: 'edit'}
  ]
});

export default router;
