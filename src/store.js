import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    clubs: [
      {
        name: "Barcelona",
        nick: "Barca",
        description:
          "Futbol Club Barcelona, commonly referred to as Barcelona and colloquially known as Barça, is a Spanish professional football club based in Barcelona, Catalonia, Spain."
      },
      {
        name: "Real Madrid",
        nick: "Los Blancos",
        description:
          "Real Madrid Club de Fútbol, commonly referred to as Real Madrid, is a Spanish professional football club based in Madrid. Founded on 6 March 1902 as Madrid Football Club, the club has traditionally worn a white home kit since inception."
      }
    ]
  },
  getters: {},
  mutations: {
    ADD_CLUB(state, payload) {
      state.clubs.push(payload);
    },
    DELETE_CLUB(state, payload) {
      const newClub = []
      state.clubs.forEach((club) => {
        if (club.name !== payload) {
          newClub.push(club)
        }
      })
      state.clubs = newClub
    },
    UPDATE_CLUB(state, {payload, updatedDetails}) {
      state.clubs.forEach(club => {
        if (club.name === payload){
          club = updatedDetails
        }
      })
    }
  }
});

export default store;
